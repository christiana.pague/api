// business logic
// including model methods

// User model
const { model } = require('mongoose')
const User = require('./../models/User')
const bcrypt = require('bcrypt')
const { JsonWebTokenError } = require('jsonwebtoken')
const { createAccessToken } = require('./../auth')

module.exports.getAllUsers = () => {
  // difference between findOne() & ficnd()
  // findOne() returns one document
  // find(query, fieldprojection) return an array of documents []
  return User.find().then((result, error) => {
    return result ? result : error
  })
}

module.exports.getUserProfile = (reqBody) => {
  const { email, password } = reqBody

  return User.findOne({ email: email }).then((result, error) => {
    // console.log(result)

    if (result == null) {
      console.log('email null')
      return false
    } else {
      return result
    }
  })
  // mini activity
  // using findById(), look for the matching docunent and
  // return the matching document to the client
}

module.exports.checkEmail = async (email) => {
  // find the matching document in the databse using email
  // by using Model() method.
  // send back the response to the client

  const result = await User.findOne({ email: email })
  // if (result != null) email.send(false)
  // return result == null ? email.send(true) : res.send(error)

  console.log(result) //null

  if (result !== null) {
    return false
  } else {
    if (result === null) {
      //send back the response to the client
      return true
    } else {
      return error
    }
  }
}

module.exports.registerUser = (reqBody) => {
  // save/create a new user document
  // using .save() method to save document to the database
  // console.log(reqBody)

  // how to use object destructuring
  // why? to make distinct variables for each property w/o using dot notation
  // const {properties} = < object reference >
  const { firstname, lastname, email, password, mobileNo, age } = reqBody
  // console.log(firstName)

  const newUser = new User({
    firstname: firstname,
    lastname: lastname,
    email: email,
    password: password,
    mobileNo: mobileNo,
    age: age
  })

  // save the newUser object to the database
  return newUser.save().then((result, error) => {
    // console.log(result)
    // return error ? error : result
    return result ? true : error
  })
}

module.exports.login = (reqBody) => {
  const { email, password } = reqBody

  return User.findOne({ email: email }).then((result, error) => {
    console.log(result)

    if (result == null) {
      console.log(`email null`)
      return false
    } else {
      let isPasswordCorrect = bcrypt.compareSync(password, result.password) // rerturns boolean

      if (isPasswordCorrect === true) {
        // return json web token
        // invoke the function in which creates the token upon logging in
        // requirements in creating a token:
        // if password matches from existing password to database
        return { access: createAccessToken(result) }
      } else {
        return false
      }
    }
  })
}

